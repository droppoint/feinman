// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "signals.js" as Signals

Rectangle {
    id: rooter
    width: 300
    height: 450

    color: "#e6e6e6"
    focus:true
    //
	
    signal load_file();
	
	//Сигнал испускается при прекращении преобразования
	signal cancel_operation();
	
	FontLoader {
		id: ubuntu
		source: "fonts/Ubuntu-L.ttf" 
	}
		
	function init_connection(){
        load_file()
	}
	

//	// эта функция принимает сигнал от
//	// модуля коннектор и в зависимости
//	// от сигнала реализует поведение
//	// интерфейса
//    function signal(status) {
//    	console.log("signal")
//        if (status == "200"){
//        	console.log('Connected signal recieved')
//        	console.log(Signals.signal_hash[status])
//        	rooter.state = "connected"
//        }
//        else if ( include(Signals.error_signals, status) ){
//        	console.log("Error signal recieved")
//        	console.log(Signals.signal_hash[status])
//        	rooter.state = ""
//        	console.log(rooter.state)
//        }
//        else if ( include(Signals.connecting_signals, status) ){
//        	console.log("Connecting signal recieved")
//        	rooter.state = "connecting"
//        	con_status.text = Signals.signal_hash[status]
//        }
//    }
//	Keys.onPressed: {
//        console.log('button pressed')
//        console.log(event.key)
//        if((event.key === Qt.Key_Enter) || (event.key === Qt.Key_Return)){
//            if(rooter.state === ""){
//                console.log('connect')
//                init_connection()
//            }
//            else {
//                console.log('disconnect')
//                cmd_disconnect("405")
//                rooter.state = ""
//            }
//        } else if (event.key === Qt.Key_Tab){
//            login_edit.focus = true
//        } else if ((event.key === Qt.Key_Escape) && (rooter.state != "")){
//            console.log('disconnect')
//            cmd_disconnect("405")
//            rooter.state = ""
//        }
//        event.accepted = true;
//    }
    
    
    Component {
    //    property string label_text: qsTr("")
        id: object_card_row
        Row {
                id: row1
                x: 0
                y: 0
                width: parent.width
                height: 24
                spacing: 0
                Text {
                    height: 24
                    color: "#424242"
                    text: label_text
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    font.bold: true
                    font.pointSize: 11
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                }
                Input {
                    width: 140
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    default_string: label_text
                }
            }

    }


    MouseArea {
        id: mousearea1
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
           // Qt.quit();
        }
			
        Flickable {
            id: control_flick
            height: 280
            flickableDirection: Flickable.HorizontalFlick
            boundsBehavior: Flickable.StopAtBounds
            contentWidth: 900
            contentHeight: 270
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            interactive: false

            Text {
                id: program_name
                color: "#424242"
                text: qsTr("Feinman")
                // renderType: Text.NativeRendering
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 50
                font.weight: Font.Light
                font.italic: false
                font.bold: false
                font.family: ubuntu.name
                font.pixelSize: 44
            }

//            Progressbar {
//                id: progress_wheel
//                anchors.left: parent.left
//                anchors.leftMargin: 420
//                anchors.top: parent.top
//                anchors.topMargin: 140
//                opacity: 0
//            }


            ListView {
                id: object_card
                x: 300
                y: 0
                width: 300
                height: 210
                contentHeight: 1000
                contentWidth: 300
                spacing: 10
                boundsBehavior: Flickable.StopAtBounds
                interactive: true
                delegate: object_card_row
                model: ListModel {
                    ListElement {
                        label_text: "Месторождение"
                    }
                    ListElement {
                        label_text: "Объект"
                    }
                    ListElement {
                        label_text: "Вариант"
                    }
                }
                opacity: 1

            }


            Button {
                id: cancel_button
                x: 380
                y: 196
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                top_color: "#EE5F5B"
                bottom_color: "#BD362F"
                default_string: "Отмена"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 44
                onBtnClicked: { cancel_operation()
                   rooter.state = ""}

            }

            Button {
                id: save_button
                x: 680
                y: 196
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                default_string: "Сохранить"
                anchors.horizontalCenterOffset: 300
                anchors.bottomMargin: 44
            }

            Button {
                id: load_file_button
                x: 83
                y: 196
                width: 140
                default_string: "Обзор"
                anchors.horizontalCenterOffset: -300
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 44
                onBtnClicked: {
                    init_connection()
                }
            }

        }

        Flickable {
            id: background_flick
            height: 380
            z: -2
            contentHeight: 380
            contentWidth: 600
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0

//            Image {
//                id: background
//                x: 0
//                y: 0
//                width: 600
//                height: 380
//                z: -1
//                source: "images/background2.png"
//            }
        }

        Text {
            id: credits
            color: "#424242"
            text: qsTr("Powered by Alexei Partilov @ 2013")
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            font.family: "DejaVu Sans"
            font.pixelSize: 10
        }

    }

    states: [
        State {
            name: "in_progress"
            PropertyChanges{ target: control_flick; contentX: "300" }
            PropertyChanges{ target: background_flick; contentX: "150" }
        },
        State {
            name: "done"
            PropertyChanges{ target: control_flick; contentX: "600" }
            PropertyChanges{ target: background_flick; contentX: "300" }
        }
        
    ]

    transitions: Transition {
        reversible: true
        PropertyAnimation {
			id: slider
            targets: [control_flick, background_flick]
            property: "contentX"
            duration: 400
            easing.type: Easing.InSine
        }
    }

}
