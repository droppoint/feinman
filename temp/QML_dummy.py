# -*- coding: UTF-8 -*-

from PySide import QtGui
from PySide import QtCore
from PySide import QtDeclarative
from PySide import QtSvg
import logging
import sys


class Interface(QtDeclarative.QDeclarativeView):
    '''
    Connects with qml interface
    '''

    def __init__(self):
        '''
        Constructor
        '''
        super(Interface, self).__init__()
        self.setSource('QML/Feinman.qml')
        logger = logging.getLogger('Feinman.Interface')
        logger.debug("Interface start")
        #Подключение сигналов
        self.rootObject = self.rootObject()
        self.setWindowTitle("Feinman")
        self.setMinimumSize(QtCore.QSize(300, 450))
        self.setMaximumSize(QtCore.QSize(300, 450))
        self.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)

        self.show()
        self.rootObject.load_file.connect(self.set_open_filename)

        self.native = QtGui.QCheckBox()
        self.native.setText("Use native file dialog.")
        self.native.setChecked(True)
#        self.rootObject.cmd_disconnect.connect(self.connector.disconnect)

#        self.createActions()
#        self.trayIcon = QtGui.QSystemTrayIcon(self)
#        self.setTrayIcon()
#        self.setWindowFlags(QtCore.Qt.WindowCloseButtonHint)

#        self.setWindowIcon(QtGui.QIcon('../QML/images/trayicon_32px.svg'))

#        self.trayIcon.activated.connect(self.iconActivated)
#        self.emit_signal("101")
#        self.trayIcon.show()

#        self.showMessage()

    def set_open_filename(self):
        options = QtGui.QFileDialog.Options()
        if not self.native.isChecked():
            options |= QtGui.QFileDialog.DontUseNativeDialog
        filename, _filtr = QtGui.QFileDialog.getOpenFileName(
            self, u"Открыть", "",
            "Eclipse DATA File (*.data);;Eclipse RSM File (*.rsm);;All Files (*)",
            "", options)
        if filename:
            return filename

if __name__ == '__main__':

    app = QtGui.QApplication(sys.argv)
    view = Interface()
    sys.exit(app.exec_())
