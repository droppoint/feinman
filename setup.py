try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Feinman',
    'author': 'Alex Partilov',
    'url': '',
    'download_url': '',
    'author_email': 'partilov@gmail.com',
    'version': '0.1',
    'install_requires': [],
    'packages': ['Feinman'],
    'scripts': [],
    'name': 'Feinman'
}

setup(**config)
