# -*- coding: UTF-8 -*-
import sys
from cx_Freeze import setup, Executable

exe = Executable(
      script="main.py",
      base="Win32GUI",
      targetName="main.exe"
     )

build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}

setup(
    name = "Project Feinman",
    version = "1.0",
    description = "Help tool for gosplan tables.",
    options = {"build_exe": build_exe_options},
    exectuables = [exe])
