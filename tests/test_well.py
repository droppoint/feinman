# -*- coding: UTF-8 -*-
'''
Created on 18.10.2012

@author: APartilov
'''
import unittest
from Feinman.oilfield.well import Well
import numpy as np


class TestWell(unittest.TestCase):

    def setUp(self):
        self.well = Well("405")

    def tearDown(self):
        self.well.clear()

    def test_add_parameter(self):
        """Тест на добавление параметров"""
        self.well.add_parameter("OIL", [0, 2, 6
            , 20])
        answer = self.well.recieve_parameters("OIL")
        self.assertEqual(answer["OIL"].tolist(), [0, 2, 6, 20])

    def test_recieve_parameter(self):
        """Тест на получение одного параметра из объекта Well"""
        self.well.add_parameter("OIL", [0, 2, 6, 20])
        self.well.add_parameter("WATER", [1, 3, 7, 21])
        self.well.add_parameter("OIL", [0, 4, 8, 20])
        answer = self.well.recieve_parameters("OIL")
        self.assertEqual(answer["OIL"].tolist(), [0, 4, 8, 20])

    def test_recieve_parameters(self):
        """Тест на получение множества параметров из Объекта Well"""
        self.well.add_parameter("OIL", [0, 2, 6, 20])
        self.well.add_parameter("WATER", [1, 3, 7, 21])
        self.well.add_parameter("OIL", [0, 4, 8, 20])
        answer = self.well.recieve_parameters("OIL", "WATER", "COND")
        for key in answer:
                answer[key] = answer[key].tolist()
        self.assertEqual(answer, {"OIL": [0, 4, 8, 20],
                                "WATER": [1, 3, 7, 21]})
        answer = self.well.recieve_parameters("OIL",
                                mask=[True, False, True, False])
        data = answer["OIL"].tolist()
        self.assertEqual(data, [0, 8])

    def test_work_time(self):
        """Тест на вычисление заготовки рабочего времени скважины"""
        self.well.add_parameter("WATER", [1, 3, 3, 3])
        self.well.add_parameter("OIL", [0, 4, 4, 4])
        self.well.add_parameter("WATER_I", [0, 0, 1, 3])
        self.well.add_parameter("OIL_I", [0, 0, 0, 0])
        work = self.well.work_time()
        for time in work:
            work[time] = work[time].tolist()
        self.assertEqual(work, {"PROD": [1, 0.0, 0.0],
                                "INJ":  [0.0, 1, 1]})
    
    def test_events(self):
        """Тест на построение горизонта событий"""
        self.well.add_parameter("WATER", [1, 4, 4, 4])
        self.well.add_parameter("OIL", [0, 4, 4, 4])
        self.well.add_parameter("WATER_I", [0, 0, 0, 3])
        self.well.build_event_horizon()
    
    def test_lifecycle(self):
        """Тест на вычисление жизненного периода скважины"""
        self.well.add_parameter("WATER", [1, 3, 4, 4])
        self.well.add_parameter("OIL", [0, 4, 4, 4])
        self.well.build_event_horizon()
        first_run, abandon = self.well.lifecycle()
        self.assertEqual(first_run["STEP"], 0)
        self.assertEqual(first_run["STATE"], "PRODUCTION")
        self.assertEqual(abandon["STEP"], 1)
        self.assertEqual(abandon["STATE"], "PRODUCTION")
        self.well.add_parameter("OIL_I", [0, 0, 0, 4])
        self.well.build_event_horizon()
        first_run, abandon = self.well.lifecycle()
        self.assertEqual(abandon["STEP"], -1)
        self.assertEqual(abandon["STATE"], None)

    def test_transfer(self):
        """Тест на вычисление переходов скважины из одного состояния
        в другое"""
        self.well.add_parameter("WATER", [1, 3, 4, 6, 8, 8, 8, 8, 8, 10, 15])
        self.well.add_parameter("OIL",   [0, 1, 2, 3, 4, 4, 4, 4, 4, 5, 6])
        self.well.add_parameter("WATER_I", [0, 0, 0, 0, 1, 2, 3, 4, 4, 4, 4])
        self.well.build_event_horizon()
        inj_to_prod = [i.step for i in self.well._events if i.type=="TO_PROD"]
        self.assertEqual(inj_to_prod, [8])
        prod_to_inj = [i.step for i in self.well._events if i.type=="TO_INJ"]
        self.assertEqual(prod_to_inj, [3])
        to_inactive = [i.step for i in self.well._events if i.type=="STOP"]
        from_inactive = [i.step for i in self.well._events if i.type=="START"]
        if len(to_inactive) > 1 :
            to_inactive = to_inactive[:-1]
        self.assertEqual(to_inactive, [6])
        self.assertEqual(from_inactive[1:], [8])

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
