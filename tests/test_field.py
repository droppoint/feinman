# -*- coding: UTF-8 -*-
'''
Created on 18.10.2012

@author: APartilov
'''
import unittest
from Feinman.oilfield.field import Field
from datetime import date


class TestField(unittest.TestCase):

    def setUp(self):
        dates = [date(2007, 01, 01), date(2007, 07, 01), date(2008, 01, 01)]
        self.field = Field(name="TESTFIELD")
        self.field.set_dates(dates)

    def tearDown(self):
        self.field.clear()
        self.field = None

    def test_add_well_data(self):
        """Тест на добавление данных о скважине"""
        self.field.add_well_data("403", {"OIL": [1, 2, 3]})
        self.field.add_well_data("403_2", {"OIL": [4, 5, 6]})
        self.field.add_well_data("403_3", {"OIL": [2, 3, 4]})
        self.field.add_well_data("402", {"OIL": [0, 2, 3]})
        self.field.add_well_data("402_2", {"OIL": [0, 0, 0, 1]})
        parameter = self.field._well_stock['402_2'].recieve_parameters("OIL")
        self.assertEqual(parameter, {}) # не должен добавляться, так как
                                        # превышается длина вектора
        parameter = self.field._well_stock['403'].recieve_parameters("OIL")
        self.assertEqual(parameter["OIL"].tolist(), [1, 2, 3])

    def test_cut_off_wells(self):
        """Тест для проверки фильтра скважин"""
        self.field.add_well_data("403", {"OIL": [1, 2, 3]})
        self.field.add_well_data("402", {"OIL": [0, 2, 3]})
        self.field.add_well_data("401", {"OIL": [0, 2, 3]})
        self.field.cut_off_wells({"401": "1", "403": "0.5"})
        self.assertEqual(self.field._well_stock.keys(), ["403", "401"])

    def test_work_time(self):
        """Тест вычисления общего рабочего времени скважин"""
        self.field.add_well_data("403", {"OIL": [0, 2, 3]})
        self.field.add_well_data("402", {"OIL": [0, 2, 2]})
        self.field.add_well_data("401", {"OIL": [0, 2, 2]})
        self.field.add_well_data("403", {"EFAC": [1, 0.5, 0.1]})
        self.field.add_well_data("402", {"EFAC": [1, 0.5, 0.1]})
        self.field.add_well_data("401", {"EFAC": [1, 0.5, 0.1]})
        work_time = self.field.work_time()["PROD"]
        self.assertEqual(work_time.tolist(), [543, 92])

    def test_production_rate(self):
        """Тест для вычисления общей добычи/закачки"""
        self.field.add_well_data("403",   {"OIL": [1, 2, 3]})
        self.field.add_well_data("403_2", {"OIL": [4, 5, 6]})
        self.field.add_well_data("403_3", {"OIL": [2, 3, 4]})
        self.field.add_well_data("402",   {"OIL": [0, 2, 3]})
        prod = self.field.production_rate("OIL", mode="YRL")
        prod2 = self.field.production_rate("OIL", mode="YRL",
                                           density=10, degree=2)
        self.assertEqual(prod, [9])
        self.assertEqual(prod2, [9000])

    def test_avg_pressure(self):
        """Тест для вычисления среднего давления по скважинам"""
        self.field.add_well_data("403", {"OIL": [0, 2, 3]})
        self.field.add_well_data("402", {"OIL": [0, 2, 2]})
        self.field.add_well_data("402", {"OIL_I": [0, 0, 2]})
        self.field.add_well_data("403", {"THP": [178, 195, 214]})
        self.field.add_well_data("402", {"THP": [168, 180, 215]})
        pressure = self.field.avg_pressure("THP")
        self.assertEqual(pressure["PROD"].tolist(), [187.5, 214])
        self.assertEqual(pressure["INJ"].tolist(), [0, 215])

    def test_well_fond(self):
        """Тест для вычисления фонда скважин"""
        self.field.add_well_data("403", {"OIL": [0, 2, 3]})
        self.field.add_well_data("402", {"OIL": [0, 2, 2]})
        self.field.add_well_data("402", {"OIL_I": [0, 0, 2]})
        self.field.add_well_data("403", {"EFAC": [1, 0.5, 0.1]})
        self.field.add_well_data("402", {"EFAC": [1, 0.5, 0.1]})
        fond = self.field.well_fond()
        self.assertEqual(fond["PROD"].tolist(), [2, 1])
        self.assertEqual(fond["INJ"].tolist(), [0, 1])

    def test_transfered_wells(self):
        """Тест для вычисления количества переходов скважин"""
        self.field.clear()
        dates = [date(2007, 01, 01), date(2007, 07, 01), date(2008, 01, 01),
                 date(2008, 07, 01), date(2009, 01, 01), date(2009, 07, 01), 
                 date(2010, 01, 01), date(2010, 07, 01), date(2011, 01, 01), 
                 date(2011, 07, 01), date(2012, 01, 01)]
        self.field = Field(name="TESTFIELD")
        self.field.set_dates(dates)
        self.field.add_well_data("301", {"WATER": [1, 3, 4, 6, 8, 8, 8, 8, 8, 10, 15]})
        self.field.add_well_data("301", {"OIL":   [0, 1, 2, 3, 4, 4, 4, 4, 4, 5, 6]})
        self.field.add_well_data("301", {"EFAC": [0.5, 0.4, 0.7, 0.9, 1.0, 1.0, 0.8,
                                         0.9, 0.7, 0.6, 0.7]})
        self.field.add_well_data("301", {"WATER_I": [0, 0, 0, 0, 1, 2, 3, 4, 4, 4, 4]})
        self.field.add_well_data("302", {"OIL":   [0, 2, 4, 4, 4, 6, 8, 8, 8, 8, 8]})
        self.field.add_well_data("302", {"EFAC": [0.5, 0.4, 0.7, 0.9, 1.0, 1.0, 0.8,
                                         0.9, 0.7, 0.6, 0.7]})
        self.field.add_well_data("302", {"WATER_I": [0, 0, 0, 0, 0, 0, 0, 0, 4, 6, 8]})
        for well in self.field._well_stock.values():
            well.build_event_horizon()
        transfer = self.field.transfered_wells()
        for key in transfer:
            transfer[key] = transfer[key].tolist()
        self.assertEqual(transfer, {"START": [0, 0, 0, 0, 1, 0, 0, 1, 1, 0],
                                    "TO_PROD":    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                                    "TO_INJ":   [0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
                                    "STOP":   [0, 1, 0, 0, 0, 1, 1, 0, 0, 0]})

    def test_completed_wells(self):
        """Тест для вычисления количества введеных/выведенных скважин"""
        self.field.add_well_data("403", {"OIL": [0, 2, 3]})
        self.field.add_well_data("402", {"OIL": [0, 2, 2]})
        self.field.add_well_data("402", {"OIL_I": [0, 0, 2]})
        self.field.add_well_data("403", {"EFAC": [1, 0.5, 0.1]})
        self.field.add_well_data("402", {"EFAC": [1, 0.5, 0.1]})
        self.field.add_well_data("404", {"OIL_I": [0, 0, 2]})
        self.field.add_well_data("404", {"EFAC": [1, 0.5, 0.1]})
        for well in self.field._well_stock.values():
            well.build_event_horizon()
        fond = self.field.completed_wells()
        self.assertEqual(fond["PROD_IN"].tolist(), [2, 0])
        self.assertEqual(fond["INJ_IN"].tolist(), [0, 1])
        self.assertEqual(fond["PROD_OUT"].tolist(), [0, 0])
        self.assertEqual(fond["INJ_OUT"].tolist(), [0, 0])

    # def test_roll_sum(self):
    #     result = roll_sum([False, True, False, True, False, False, True],
    #                        np.array([2, 1, 3, 4, 8, 8, 10]))
    #     self.assertEqual(result.tolist(), [3, 7, 26])


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
