# -*- coding: UTF-8 -*-

import unittest
from Feinman.parsers.data_parser import DataParser


class Test(unittest.TestCase):

    def tearDown(self):
        self.parser.close()

    # def test_custom_words(self):
    #     self.parser = DataParser(
    #                  'test_materials/data/test_custom_words.data')
    #     lines = []
    #     line = self.parser._next_line()
    #     while line:
    #         lines.append(line)
    #         line = self.parser._next_line()
    #     self.assertEqual(lines, [ "NOECHO", "95 /",
    #                               "SOME", "830 324 /", "/",  "END"])

    def test_commentaries(self):
        """Тест на распознавание комментариев"""
        self.parser = DataParser('test_materials/data/test_commentary.data')
        lines = [i for i in self.parser._next_line()]
        self.assertEqual(lines, ['NOECHO 95 /', 'ECHO', 'SOME CHORDS'])

    def test_end(self):
        """Тест на распознавание конца файла"""
        self.parser = DataParser('test_materials/data/test_end.data')
        lines = [i for i in self.parser._next_line()]
        self.assertEqual(lines, ['NOECHO 95 /', 'ENDSOMETHING'])

    def test_blocks(self):
        """Тест на распознавание блоков"""
        self.parser = DataParser('test_materials/data/test_blocks.data')
        blocks = {}
        for block in self.parser.next_block():
            blocks.update(block)
        self.assertEqual(blocks, {'NOECHO': [['96']],
                                  'SOME': [['830', '324']],
                                  'WORD': [],
                                  'NUMERIC': [['13', '12'], ['14', '15'],
                                              ['16', '17']]
                                  })

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
