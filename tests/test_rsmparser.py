# -*- coding: UTF-8 -*-
'''
Created on 18.10.2012

@author: APartilov
'''
import unittest
from Feinman.parsers.rsm_parser import RSMParser


class TestRSMParser(unittest.TestCase):

    def tearDown(self):
        self.parser.close()

    def test_read_delimiter(self):
        """Тест на распознавание ограничителя блоков"""
        self.parser = RSMParser('test_materials/rsm/eclipse.rsm')
        delimiter = self.parser._read_delimiter_format()
        self.assertEqual(delimiter, "\r\n\r\n")
        self.parser.close()

    def test_block_borders(self):
        """Тест на вычисление границ блоков"""
        self.parser = RSMParser('test_materials/rsm/eclipse.rsm')
        start, end = self.parser._find_block_borders()
        self.assertEqual(start, 481)
        self.assertEqual(end, 29819)

    def test_dates(self):
        """Тест на распознавание дат"""
        import datetime
        self.parser = RSMParser('test_materials/rsm/eclipse_short.rsm')
        dates = [datetime.datetime(2006, 1, 1, 0, 0),
                 datetime.datetime(2006, 2, 1, 0, 0),
                 datetime.datetime(2006, 3, 1, 0, 0)]
        self.assertEqual(dates, self.parser.dates)

    def test_block(self):
        """Тест на распознавание строк с данными"""
        self.parser = RSMParser('test_materials/rsm/eclipse.rsm')
        self.assertTrue(self.parser._next_block_exists())
        for line in self.parser._read_data_block():
            self.assertEqual(len(line), 9)

    def test_header(self):
        """Тест распознавания заголовков"""
        self.parser = RSMParser('test_materials/rsm/eclipse.rsm')
        header = self.parser._read_header()
        self.assertEqual(header,
                        ["YEARS", "DAY", "MONTH", "YEAR", "FMWPT",
                         "FMWPR", "FMWIT", "FMWIN", "FLPRH"])

    def test_context(self):
        """Тест распознавания строк с контекстом(имена скважин и степени)"""
        self.parser = RSMParser('test_materials/rsm/eclipse_factors.rsm')
        numbers, factors = self.parser._read_context()
        self.assertEqual(numbers, ['K62', 'K61', 'K62', 'K61',
                                   'K62', 'K61', 'K62', 'K61', 'K62'])
        self.assertEqual(factors, ['3', '3', '3', '3', '3', '3', '3', '', ''])

    def test_read_file(self):
        """Сырой тест распознавания всего файла(прежде всего не должен 
            вызвать exception)"""
        self.parser = RSMParser('test_materials/rsm/eclipse_parse_file.rsm')
        for _block in self.parser.next_block():
            pass


class TestRSMParserRoxar(unittest.TestCase):

    def tearDown(self):
        self.parser.close()

    def test_read_delimiter(self):
        """Тест на распознавание ограничителя блоков"""
        self.parser = RSMParser('test_materials/rsm/roxar.rsm')
        delimiter = self.parser._read_delimiter_format()
        self.assertEqual(delimiter, "\r\n1\r\n")
        self.parser.close()

    def test_block_borders(self):
        """Тест на вычисление границ блоков"""
        self.parser = RSMParser('test_materials/rsm/roxar.rsm')
        start, end = self.parser._find_block_borders()
        self.assertEqual(start, 296)
        self.assertEqual(end, 91138)

    def test_dates(self):
        """Тест на распознавание дат"""
        import datetime
        self.parser = RSMParser('test_materials/rsm/roxar_short.rsm')
        dates = [datetime.datetime(2000, 1, 1, 0, 0),
                 datetime.datetime(2000, 3, 1, 0, 0),
                 datetime.datetime(2000, 5, 1, 0, 0)]
        self.assertEqual(dates, self.parser.dates)

    def test_block(self):
        """Тест на распознавание строк с данными"""
        self.parser = RSMParser('test_materials/rsm/roxar.rsm')
        self.assertTrue(self.parser._next_block_exists())
        for line in self.parser._read_data_block():
            self.assertEqual(len(line), 9)

    def test_header(self):
        """Тест распознавания заголовков"""
        self.parser = RSMParser('test_materials/rsm/roxar.rsm')
        header = self.parser._read_header()
        self.assertEqual(header,
                        ["WLPRH", "WLPT", "WMODE",
                         "WOIRH", "WOPR", "WOPRH",
                         "WOPT", "WPDD", "WSTA"])

    def test_context(self):
        """Тест распознавания строк с контекстом(имена скважин и степени)"""
        self.parser = RSMParser('test_materials/rsm/roxar_factors.rsm')
        numbers, factors = self.parser._read_context()
        self.assertEqual(numbers, ['338_2', '338_2', '338_2',
                                   '338_2', '338_2', '338_2',
                                   '338_2', '338_2', '338_2'])
        self.assertEqual(factors, ['', '', '', '3', '', '', '', '', ''])

    # def test_read_file(self):
    #     self.parser = RSMParser('test_materials/rsm/roxar_big.rsm')
    #     for _block in self.parser.parse_file():
    #         pass

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
