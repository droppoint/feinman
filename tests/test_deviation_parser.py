# -*- coding: UTF-8 -*-

import unittest
from Feinman.parsers.deviation_parser import DevParser


class TestDevParser(unittest.TestCase):

    def tearDown(self):
        self.parser.close()

    def test_init(self):
        self.parser = DevParser('test_materials/dev/test.dev')
        self.parser.close()

    def test_block_count(self):
        """Тест на распознавание блоков. Определение количества"""
        self.parser = DevParser('test_materials/dev/test.dev')
        i = 0
        while self.parser._next_block_exists():
            i += 1
        self.assertEqual(i, 2)

    def test_line_count(self):
        """Тест на правильное считывание строк данных. 
        Определение количества"""
        self.parser = DevParser('test_materials/dev/test.dev')
        i = 0
        for _line in self.parser._next_line():
            i += 1
        self.assertEqual(i, 597)

    def test_parse_block(self):
        """Тест распознавания блока. Валидность заголовков"""
        self.parser = DevParser('test_materials/dev/test.dev')
        block = self.parser.next_block().next()
        self.assertEqual(block.keys(), ["Z-45"])

    def test_parse_file(self):
        """Тест распознавания файла. Валидность заголовков"""
        self.parser = DevParser('test_materials/dev/test.dev')
        blocks = {}
        for block in self.parser.next_block():
            blocks.update(block)
        self.assertEqual(blocks.keys(), ["Z-44ST2", "Z-45"])

    def test_no_delimiter(self):
        """Тест распознавания поврежденного файла. 
        Отсутствие ограничителя блока"""
        self.parser = DevParser('test_materials/dev/test(no_delimiter).dev')
        blocks = {}
        for block in self.parser.next_block():
            blocks.update(block)
        self.assertEqual(blocks.keys(), ["Z-44ST2", "Z-45"])

    def test_no_wellname(self):
        """Тест распознавания поврежденного файла. Отсутствие заголовка"""
        self.parser = DevParser('test_materials/dev/test(no_well_name).dev')
        blocks = {}
        for block in self.parser.next_block():
            blocks.update(block)
        self.assertEqual(blocks.keys(), ["Z-44ST2"])

    def test_data_shortage(self):
        """Тест распознавания поврежденного файла. 
        Частичное отсутствие данных"""
        self.parser = DevParser('test_materials/dev/test(data_shortage).dev')
        blocks = {}
        for block in self.parser.next_block():
            blocks.update(block)
        well = blocks["Z-44ST2"]
        self.assertEqual(well[2].tolist(), [-18.2, 2319.6])

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
