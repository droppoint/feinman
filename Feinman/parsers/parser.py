# -*- coding: UTF-8 -*-

'''Родительский класс парсера от которого наследуются
все остальные в пакете Feinman. Построен по большей части
для соблюдения интерфейса парсеров.

@author: Alexei Partilov
'''

from __future__ import absolute_import

from abc import ABCMeta, abstractmethod


class AbstractParser:
    '''
    Абстрактный класс парсеров
    '''
    __metaclass__ = ABCMeta

    @abstractmethod
    def next_block(self):
        '''
        Метод должен выдавать следующий блок из
        читаемого файла.
        '''
        pass
