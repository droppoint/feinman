# -*- coding: UTF-8 -*-
'''Класс DataParser предназначен для парсинга файлов
Shlumberger Eclipse .data.
Парсинг может осуществляться только методом
"сверху-вниз", без произвольного перемещения внутри
файла.

@author: Alexei Partilov
@version: 0.3
'''

from __future__ import absolute_import

import logging
from Feinman.parsers.parser import AbstractParser


class DataParser(AbstractParser):

    def __init__(self, filename):
        import mmap
        import re
        # Инициализация журнала
        self.__logger = logging.getLogger('Feinman.parsers.DataParser')
        self.__filename = filename
        try:
            self.__file = open(self.__filename, mode='r+')
            self.__buf = mmap.mmap(self.__file.fileno(), 0)
        except IOError:
            self.__logger.error("File not found: " + filename)
            raise
        self.__pointer = 0  # можно удалить за ненадобностью
        self.__buf.seek(self.__pointer)
        self.__commentary_pattern = re.compile(r'(^\s*--)|(^\s*$)')
        self.__end_pattern = re.compile(r"^\s*END(\s*|\s*[#]\w*)$")

        self.__keyword_pattern = re.compile(r"^[a-zA-Z]+$")
        # self.__custom_word_pattern = re.compile(r'(^\s*--#\w+)')  (?!#\w+)

    def _next_line(self):
        '''Возвращает следующую стоку из
        файла, при условии что это не комментарий
        и не пустая строка.
        '''
        for line in iter(self.__buf.readline, ""):
            if self.__commentary_pattern.match(line):
                continue
            if line.find('--') != -1:
                line = line[:line.find('--')]
            if self.__end_pattern.match(line):
                break
            if line:
                yield line.strip()

    def next_block(self):
        """Основной публичный метод. Генератор возвращает следующий
        чистый блок из файла.
        """
        data = []
        keyword = None
        for line in self._next_line():
            if self.__keyword_pattern.match(line):
                # Ключевое слово
                if keyword:
                    # Ключевое слово без данных, либо без
                    # закрывающего слеша
                    yield {keyword.upper(): data}
                    keyword = None
                    data = []
                keyword = line.upper()
                data = []
            elif (line == "/") and keyword:
                # Закрывающий слеш
                yield {keyword: data}
                keyword = None
                data = []
            elif keyword:
                # Данные
                line = line.strip("/")
                data.append(line.split())
        yield {keyword: data}

    def close(self):
        '''Закрытие файла и очистка буфера.
        ОБЯЗАТЕЛЬНО К ВЫПОЛНЕНИЮ ПОСЛЕ ЧТЕНИЯ ФАЙЛА.
        '''
        self.__buf.close()
        self.__file.close()
