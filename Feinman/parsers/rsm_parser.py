# -*- coding: UTF-8 -*-

'''Предназначен для парсинга файлов .RSM из пакетов
Shlumberger Eclipse и Roxar Tempest
Парсинг осуществляется сверху-вниз поблочно.

@author: Alexei Partilov
@version: 0.5
'''

from __future__ import absolute_import

import re
import os
import logging
import json

from Feinman.parsers.parser import AbstractParser


CUR_DIR = os.path.abspath(os.path.dirname(__file__))
CONFIG_DIR = os.path.abspath(os.path.join(os.path.dirname(CUR_DIR), 'config'))
# Regular expressions used for parsing
regex_all_numbers = re.compile(r"\s([A-Za-z0-9][\w\-]*)\s")
regex_factor = re.compile(r"(?:\*10\*\*(\d))")
regex_date_numeric = re.compile(r"((?:0[1-9]|[1-2][0-9]|3[0|1])/"
                 "(?:0[1-9]|1[0-2])/"
                "(?:(?:19|20|21|22)\d{2}))")
regex_date_alphabetic = re.compile(r"((?:0[1-9]|[1-2][0-9]|3[0|1])-"
                                      "(?:[ADFJMNOS][A-Za-z]{2})-"
                                      "(?:(?:19|20|21|22)\d{2}))")


def listmerge(lst):
    """Слияние списков различной степени вложенности в один список
    Применяется для подгрузки имен необходимых векторов из json файла"""
    res = []
    for el in lst:
        res += listmerge(el) if isinstance(el, list) else [el]
    return res


# Считывание имен необходимых векторов из json файла
def total_parameters(path):
    # total_parameters = []
    mas = {}
    with open(path, 'r+') as json_file:
        try:
            mas = json.load(json_file)
        except ValueError:
            raise ValueError("Ошибка при считывании  \
                            файла конфигурации векторов")
        # for object_type in mas.values():
        #     for vector_type in object_type.values():
        #         total_parameters.append(vector_type)
    return mas  # listmerge(total_parameters)


class ParseError(Exception):
    """Exception raised for all parse errors."""

    def __init__(self, msg, position=(None, None)):
        assert msg
        self.msg = msg
        self.lineno = position[0]
        self.offset = position[1]

    def __str__(self):
        result = self.msg
        if self.lineno is not None:
            result = result + " at line %d" % self.lineno
        if self.offset is not None:
            result = result + " column %d" % (self.offset + 1)
        return result


class RSMParser(AbstractParser):

    def __init__(self, filename):
        import mmap
        self.__logger = logging.getLogger('Feinman.parsers.RSMParser')
        self._pointer = 0
        try:
            self.__file = open(filename, mode='r+')
            self._buf = mmap.mmap(self.__file.fileno(), 0)
        except IOError:
            self.__logger.error("File not found: " + filename)
            raise
        self._buf.seek(self._pointer)
        # Определение формата даты
        self._delimiter = self._read_delimiter_format()
        self._mas = total_parameters(CONFIG_DIR + '/vectors.json')

        # REFACTOR THIS
        self._nec_parameters = [i for i in self._mas["Field"].values()]
        self._nec_parameters += [i for i in self._mas["Well"].values()]
        self._use_tracers = False
        self._tracers = [i for i in self._mas["Tracers"].values()]

        self._date_pattern, self._date_pattern_str = self._read_date_format()
        self.dates = self._read_dates()

    def report_progress(self):
        return self._pointer * 100 // self._buf.size()

    def _read_delimiter_format(self):
        '''Считывает формат разделителя между блоками
        '''
        self._buf.seek(0)
        delimiter = self._buf.readline()
        if delimiter not in ("\r\n", '1\r\n'):
            self.__logger.error('Unknown delimiter' + delimiter)
            raise ParseError('Unknown delimiter')
        self._buf.seek(self._pointer)
        return '\r\n' + delimiter

    def _find_block_borders(self):
        '''Находит границы блока
        '''
        start = self._buf.find("DATE", self._pointer)
        end = self._buf.find(self._delimiter, start)
        if end == -1:
            end = self._buf.size()
        return start, end

    def _read_dates(self):
        '''Считывает список дат из файла
        '''
        from datetime import datetime
        self._buf.seek(self._pointer)
        start, end = self._find_block_borders()
        self._buf.seek(start)
        dates = []
        while not self._buf.tell() >= end:
            line = self._buf.readline()
            if self._date_pattern.search(line):
                clear_line = self._date_pattern.search(line).group(0)
                date = datetime.strptime(clear_line, self._date_pattern_str)
                dates.append(date)
        self._buf.seek(self._pointer)
        return dates

    def _read_date_format(self):
        '''Считывает формат даты из файла
        '''
        start, end = self._find_block_borders()
        self._buf.seek(start)
        while True:
            line = self._buf.readline()
            if regex_date_numeric.search(line):
                return regex_date_numeric, "%d/%m/%Y"
            elif regex_date_alphabetic.search(line):
                return regex_date_alphabetic, "%d-%b-%Y"
            elif self._buf.tell() > end:
                self.__logger.error('Unknown date type of file damaged')
                raise ParseError('Unknown date type or file damaged')
        self._buf.seek(self._pointer)

    def _read_header(self):
        '''Считывает строку с заголовками,
        разделяет в список и фильтрует.
        '''
        n = self._buf.find("DATE", self._pointer)
        self._buf.seek(n)
        tmp = self._buf.readline()
        tmp = tmp.split()
        tmp.pop(0)  # Removing Date
        self._buf.seek(self._pointer)
        return tmp

    def _next_block_exists(self):
        ''' Если есть следующий блок для чтения перемещает
        указатель на начало следующего блока и возращает
        True
        '''
        self._pointer += 1
        n = self._buf.find("SUMMARY", self._pointer)
        if n == -1:
            return False
        self._buf.seek(n)
        self._pointer = n
        return True

    def _read_data_block(self):
        ''' Генератор, возвращает построчно, в виде массива
        отфильтрованные данные.
        '''
        start, end = self._find_block_borders()
        self._buf.seek(start)
        while not self._buf.tell() >= end:
            line = self._buf.readline()
            if not re.match(r'^\s*$|^&', line) and \
                self._date_pattern.search(line):
                line = line.split()
                line.pop(0)
                yield line
        self._buf.seek(self._pointer)

    def _read_context(self):
        '''Считывает номера и степени из заголовка,
        иначе возвращает None.
        '''
        self._buf.seek(self._pointer)
        start = self._buf.find("DATE", self._pointer)
        self._buf.seek(start)
        self._buf.readline()
        self._buf.readline()  # skipping quantities
        numbers, factors = None, None
        while True:
            line = self._buf.readline()
            if self._date_pattern.search(line):
                break    # слабое место (нет ограничений)
            elif regex_all_numbers.search(line):
                numbers = line
            elif regex_factor.search(line):
                factors = line
        self._buf.seek(self._pointer)
        if numbers:
            numbers = strip_line(numbers)
        if factors:
            factors = strip_line(factors)
            factors = [i.strip('*10**') for i in factors]
        return numbers, factors

    def _translate_header(self, header):
        if (header == "") or (header == None):
            return header
        for obj_type in self._mas.values():
            for parameter_type in obj_type:
                if header == obj_type[parameter_type]:
                    return parameter_type

    def next_block(self):
        '''Метод проходит по файлу и выдает блоки распарсенных данных.
        Является генератором.
        '''
        import math
        yield {"RSM_DATES": self.dates}
        while self._next_block_exists():
            header = self._read_header()
            if (not self._use_tracers) and (set(header) & set(self._tracers)):
                self._use_tracers = True
                self._nec_parameters = self._mas["Well"]
                self._nec_parameters.update(self._mas["Tracers"])
                self._nec_parameters = \
                    [i for i in self._nec_parameters.values()]
                self._nec_parameters += \
                    [i for i in self._mas["Field"].values()]
                # print self._nec_parameters

            # Если в header нет ничего нам необходимого, то двигаемся
            # к следующему блоку
            if not (set(header) & set(self._nec_parameters)):
                continue

            # Считываются номера скважин, степень(если есть)
            # и сам блок данных
            numbers, factors = self._read_context()
            block = self._read_data_block()

            # Выбираем необходимые нам уникальные заголовки столбцов
            # и делаем список index с индексами этих столбцов
            clear_headers = list(set(header) & set(self._nec_parameters))
            index = []
            for i in clear_headers:
                index += indices(header, i)

            # Создаем список с необходимыми именами столбцов (не уникальные)
            clear_headers = [header[i] for i in index]
            # Считываем блок данных. Конвертируем в float. Транспонируем
            clear_block = []
            for line in block:
                clear_block.append([float(line[i]) for i in index])
            clear_block = zip_list(*clear_block)
            clear_block = [i for i in clear_block]
            # Если есть номера (скважин региона и т.д.), то выбираем
            # необходимые. Если нет создаем соразмерную пустышку.
            if numbers:
                clear_numbers = [numbers[i] for i in index]
            else:
                clear_numbers = ['' for i in index]
            # Если есть степени, то вытаскиваем необходимые и
            # умножаем на них данные
            if factors:
                clear_factors = [factors[i] for i in index]
                for num, factor in enumerate(clear_factors):   # костыль
                    if factor != '':
                        clear_block[num] = \
                        [math.pow(10, float(factor)) *
                         i for i in clear_block[num]]
            # Формируем блоки распарсенных данных и отправляем
            # страждущим
            for num, parameter in enumerate(clear_headers):
                parsed_data = {}
                parsed_data['number'] = clear_numbers[num]
                parsed_data['data'] = clear_block[num]
                parameter = self._translate_header(parameter)
                parsed_data['parameter'] = parameter
                yield parsed_data
        self.close()

    def close(self):
        '''Закрывает буфер и файл
        Выполнять обязательно после окончания работ с файлом
        '''
        self._buf.close()
        self.__file.close()


def strip_line(string):
    result = string[14:]
    result = [i.strip() for i in split_by_n(result, 13)]
    return result


def zip_list(*iterables):
    # izip('ABCD', 'xy') --> Ax By
    iterators = map(iter, iterables)
    while iterators:
        yield list(map(next, iterators))


def indices(mylist, value):
    return [i for i, x in enumerate(mylist) if x == value]


def split_by_n(seq, n):
    """A generator to divide a sequence into chunks of n units."""
    while seq and len(seq) >= n:
        yield seq[:n]
        seq = seq[n:]
