# -*- coding: UTF-8 -*-

'''Предназначен для парсинга файлов .dev из пакета
Shlumberger Eclipse

@author: Alexei Partilov
@version: 0.6
'''

from __future__ import absolute_import

import logging
import numpy as np
from Feinman.parsers.parser import AbstractParser


class DevParser(AbstractParser):

    _DELIMITER = "-999"
    _WELL_HEADER = "WELLNAME:"

    def __init__(self, filename):
        import mmap
        import re
        # Инициализация журнала
        self.__logger = logging.getLogger("Feinman.parsers.DevParser")
        self.__filename = filename
        try:
            self.__file = open(self.__filename, mode="r+")
            self._buf = mmap.mmap(self.__file.fileno(), 0)
        except IOError:
            self.__logger.error("File not found: " + filename)
            raise IOError("File not found: " + filename)
        self._pointer = 0
        self._buf.seek(self._pointer)
        self._commentary_pattern = re.compile(r'(^\s*--)|(^\s*$)')
        # self.__custom_word_pattern = re.compile(r'(^\s*--#\w+)')  (?!#\w+)

    def _next_block_exists(self):
        """Если есть следующий блок для чтения перемещает
        указатель на начало следующего блока и возращает
        True."""
        n = self._buf.find(self._WELL_HEADER, self._pointer)
        if n == -1:
            return False
        self._buf.seek(n)
        self._pointer = n + 1
        return True

    def _next_line(self):
        """ГЕНЕРАТОР Возвращает следующую строку из
        файла, при условии, что это не комментарий
        и не пустая строка.
        """
        for line in iter(self._buf.readline, ""):
            if self._commentary_pattern.match(line):
                continue
            if line.find('--') != -1:
                line = line[:line.find('--')]
            if line:
                yield line.strip()

    def next_block(self):
        """Парсинг блока с данными, основная процедура, которая отличает
        заголовки от данных и выгружает данные в
        виде двумерного массива numpy."""
        wellname = None
        welldata = []

        for line in self._next_line():
            line = line.split()
            if self._WELL_HEADER in line:
                if wellname:
                    """Если попалось новое имя скважины, а разделителя
                    при этом не было, значит файл поврежден,
                    но не критично.
                    Запись в лог и выгружаем то, что есть."""
                    self.__logger.info("Delimiter is missed at: "
                                       + str(self._buf.tell())
                                       )
                    welldata = np.array(welldata).transpose()
                    yield {wellname: welldata}
                    wellname = None
                    welldata = []
                if len(line) == 1:
                    """Это говорит нам о том, что имя скважины по
                    какой-то причине пропущено, следовательно
                    пропускаем блок."""
                    self.__logger.info("Wellname is missed at: "
                                       + str(self._buf.tell())
                                       )
                    self.__logger.debug("Skipping corrupted block")
                    wellname = ''
                    self._next_block_exists()
                    continue
                wellname = line[1].strip("'")
            elif len(line) < 4:
                """Если в строке данных менее 4-х чисел, значит строка
                повреждена.
                Отбрасываем ее с занесением в лог."""
                self.__logger.info("Corrupted string at: "
                                   + str(self._buf.tell())
                                   )
                self.__logger.debug("Skipping corrupted string")
                continue
            else:
                """Данные которые не принадлежат к какой-либо скважине.
                Скорее всего мусор, пропускаем с занесением в лог."""
                if not wellname:
                    self.__logger.info("Orphan string at: "
                                       + str(self._buf.tell())
                                       )
                    self.__logger.debug("Skipping orphan string")
                    continue
                welldata.append(map(float, line))
        welldata = np.array(welldata).transpose()
        yield {wellname: welldata}

    def close(self):
        '''Закрытие файла и очистка буфера.
        ОБЯЗАТЕЛЬНО К ВЫПОЛНЕНИЮ ПОСЛЕ ЧТЕНИЯ ФАЙЛА.
        '''
        self._buf.close()
        self.__file.close()
