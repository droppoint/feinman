# -*- coding: UTF-8 -*-
'''
Created on 04.07.2012

@author: APartilov
'''

from __future__ import absolute_import

import numpy as np
import logging
from itertools import izip

from Feinman.helpers.typechecking import accepts, returns


class WellEvent(object):
    '''
    Событие при работе скважины

    Поля:
        type: тип события (остановка, запуск, перевод в добычу,
    перевод в нагнетание)
        step: шаг на котором произошло событие
    '''
    possible_types = ["START", "STOP", "TO_PROD", "TO_INJ"]
    possible_work_modes = ["PRODUCTION", "INJECTION", "STANDBY"]

    @accepts(object, str, str, int)
    def __init__(self, work_mode, event_type, step):
        # defender
        if event_type not in WellEvent.possible_types:
            raise ValueError("Unknown event type")
        if work_mode not in WellEvent.possible_work_modes:
            raise ValueError("Unknown work mode")
        else:
            self.work_mode = work_mode
        self.type = event_type
        self.step = step

    def __repr__(self):
        return "WellEvent('%s', '%s', %d)" % (self.work_mode,
                                              self.type, self.step)

    def __str__(self):
        return "%s %s at step %d" % (self.type, self.work_mode, self.step)

    def __lt__(self, other):
        if self.step == other.step:
            if self.type == "START":
                return True
            else:
                return False
        else:
            return self.step < other.step


class Well(object):
    '''
    Well  - абстракция скважины, содержит данные о добыче различных флюидов
    и давлении.

    Поля:
        name: имя скважины
        v_length: длина добавляемых векторов(вычисляется при добавлении
        первого вектора)
        parameters: вектора с данными (вектора заранее определены)
    '''
    @accepts(object, str)
    def __init__(self, name):  # number
        self.name = name
        self.v_length = 0
        self.parameters = {"OIL": None, "COND": None, "OIL_TRACE": None,
                           "WATER": None, "SGAS": None, "FGAS": None,
                           "GAS_TRACE": None, "OIL_I": None, "COND_I": None,
                           "WATER_I": None, "SGAS_I": None, "FGAS_I": None,
                           "THP": None, "BHP": None, "BP9": None,
                           "EFAC": None}
        self._events = []
        self.__logger = logging.getLogger('Feinman.oilfield.Well')
        self.changed = True
        self._work_time = {"PROD": None, "INJ": None}

    @accepts(object, (str, unicode),  list)
    def add_parameter(self, parameter, data):
        """
        Прием параметров скважины.

        parameter: строка с названием параметра
        data: вектор с данными
        """
        if parameter in self.parameters:
            if self.v_length == 0:
                self.v_length = len(data)
            elif len(data) != self.v_length:
                self.__logger.debug("Некорректная длинна вектора " +
                                    parameter + ": " + str(len(data)) +
                                    "корректная длина: " + str(self.v_length))
            self.parameters[parameter] = np.array(data)
            self.changed = True

    @accepts(object, int)
    def reduce_rates(self, n):
        """
        Пропорциальное уменьшение векторов добычи флюидов.
        Применяется при разделении на категории запасов

        n: коэффициент от 0 до 1
        """
        rates = ["OIL", "COND", "WATER", "SGAS", "FGAS", "OIL_I",
                "COND_I", "WATER_I", "SGAS_I", "FGAS_I"]
        for parameter in self.parameters:
            if (self.parameters[parameter] != None) & (parameter in rates):
                self.parameters[parameter] *= n

    @returns(dict)
    def recieve_parameters(self, *args, **kw):
        """
        Получение параметров из всех стволов скважины.
        В случае если от разных стволов приходят различные
        по размеру списки параметров, выдается ошибка.

        Сжатие всех параметров скважины по заранее заданной маске.
        Это необходимо для избавления от промежуточных данных
        (всё кроме января)
        Пример:
        [1, 2, 3, 4] + [True, False, True, False] -> [1, 3]
        """
        datalist = {}
        mask = kw.get("mask", None)
        for parameter in args:
            data = self.parameters.get(parameter, None)
            if data == None:
                continue
            if mask:
                data = np.compress(mask, data)
            if data != None:
                datalist.update({parameter: data})
        return datalist

    def build_event_horizon(self):
        """
        Построение горизонта событий
        """
        work_time = self.work_time()
        states = []
        for prod, inj in izip(work_time["PROD"], work_time["INJ"]):
            if (inj >= prod) and (inj > 0):
                states.append("INJECTION")  # скважина в закачке
            elif prod > inj:
                states.append("PRODUCTION")   # скважина в добыче
            else:
                states.append("STANDBY")   # скважина стоит
        next_states = states[1:]
        states = states[:-1]
        state = "STANDBY"
        i = 0
        for current, next_i in izip(states, next_states):
            if (current != "STANDBY") and (next_i == "STANDBY"):
                self._events.append(WellEvent(current, "STOP", i))
            elif (current == "STANDBY") and (next_i != "STANDBY"):
                self._events.append(WellEvent(next_i, "START", i + 1))
            if (current != "STANDBY") and (state == "STANDBY"):
                self._events.append(WellEvent(current, "START", i))
                state = current
            if (next_i == "INJECTION") and (state == "PRODUCTION"):
                self._events.append(WellEvent(next_i, "TO_INJ", i + 1))
                state = "INJECTION"
            elif (next_i == "PRODUCTION") and (state == "INJECTION"):
                self._events.append(WellEvent(next_i, "TO_PROD", i + 1))
                state = "PRODUCTION"
            i += 1
        self._events = sorted(self._events)

    @returns(dict)
    def work_time(self):
        """
        Выдача коэффициентов эксплуатации в зависимости от роли
        скважины. Например если по дебитам скважина работала в режиме
        добычи 1-ый и 3-ий месяц, а в режиме закачки 2-й месяц, то
        функция выдаст:
        {"PROD": [1, 0, 1],
        "INJ": [0, 1, 0]}
        При условии, что коэффициент эксплуатации всегда равен 1.
        """
        if not self.changed:
            return self._work_time
        prod = self.recieve_parameters("OIL", "COND", "WATER",
                                           "SGAS", "FGAS")
        inj = self.recieve_parameters("OIL_I", "COND_I", "WATER_I",
                                            "SGAS_I", "FGAS_I")
        prod_sum = np.zeros(self.v_length - 1)
        inj_sum = np.zeros(self.v_length - 1)
        result = {}
        result["PROD"] = np.zeros(self.v_length - 1)
        result["INJ"] = np.zeros(self.v_length - 1)
        if prod:
            prod_sum = np.array([roll_out(el) for el in prod.values()])
            prod_sum = prod_sum.sum(axis=0)
        if inj:
            inj_sum = np.array([roll_out(el) for el in inj.values()])
            inj_sum = inj_sum.sum(axis=0)
        result["PROD"] = np.where((prod_sum > 0) & (inj_sum == 0), 1, 0)
        result["INJ"] = np.where(inj_sum > 0, 1, 0)
        self._work_time = result
        self.changed = False
        return result

    @returns(tuple)
    def lifecycle(self):
        """
        Выдает этапы начала и окончания работы скважины в виде кортежа
        (начало_работы, конец_работы). В случае если скважина до сих пор
        работает в качестве конца работы указывается -1.

        Возвращает кортеж со статусами начала и окончания работы скважины
        Например,
        ({"STATE":"PRODUCTION", "STEP":0}, {"STATE":"INJECTION", "STEP":10})
        """
        end = {"STEP": -1, "STATE": None}
        if not self._events:
            raise ValueError
        start_candidate = self._events[0]  # Потому что events отсортированы
        abandon_candidate = self._events[-1]
        if start_candidate.type != "START":
            raise ValueError
        start = {"STEP": start_candidate.step,
                "STATE": start_candidate.work_mode}
        if abandon_candidate.type == "STOP":
            end = {"STEP": abandon_candidate.step,
                   "STATE": abandon_candidate.work_mode}
        return start, end

    def clear(self):
        """Очистка объекта Well"""
        self.name = None
        self.parameters.clear()
        self.work_time = None


@returns(np.ndarray)
@accepts(np.ndarray)
def roll_out(np_lst):
    np_lst = np.roll(np_lst, -1) - np_lst
    np_lst = np.delete(np_lst, -1)
    return np_lst
