# -*- coding: UTF-8 -*-
'''
Created on 24.04.2012

@author: APartilov
'''
from __future__ import absolute_import

import numpy as np
import logging
from datetime import datetime

from Feinman.oilfield.well import Well
from Feinman.helpers.typechecking import accepts, returns


class Field(object):
    """
    Field - абстракция месторождения, содержащая данные о скважинах
    за определенный отрезок времени

    Поля:
        name: имя объекта
        _well_stock: словарь содержащий все объекты Well принадлежащие
    месторождению
        parameters: векторы с данными (векторы заранее определены)
        groups: иерархия групп скважин
        _dates: список дат принадлежащих к определенному этапу
        _time_delta: список временных промежутков между этапами(строится
    на базе _dates).
    """

    @accepts(object, str)
    def __init__(self, name):
        """
        Инициализация месторождения.

        name: строка с именем месторождения
        """
        self.name = name
        self._well_stock = {}
        self.parameters = {"FIELDNAME": self.name, "OBJECTNAME": None,
                           "VERSION": None, "CATEGORY": None,
                           "OILIP": None,   "CONDIP": None,
                           "FGASIP": None,  "SGASIP": None,
                           "B_OIL": None,   "B_GAS": None,
                           "PRP": None}
        self.groups = {}
        # if not dates:
        #     raise ValueError("Field dateline is not initialised")
        self._dates = None
        self._time_delta = None
        self.__logger = logging.getLogger('Feinman.oilfield.Field')

    @accepts(object, list)
    def set_dates(self, dates):
        """
        Установка списка дат и списка временных промежутков.
        Необходимо для вычисления различных годовых характеристик,
        например, годовой добычи.

        dates: список дат
        """
        # Защита
        for date in dates:
            if not isinstance(date, datetime):
                raise TypeError("name must be datetime, not " + type(date))

        self._dates = dates
        d1 = dates[1:]
        d2 = dates[:-1]
        self._time_mask = [True if (date.month == 1) and (date.day == 1) \
                           else False for date in self._dates]
        self._roll_sum_mask = [1 if x == True else 0 for x in self._time_mask]
        self._roll_sum_mask = np.array(self._roll_sum_mask)
        self._roll_sum_mask = self._roll_sum_mask.cumsum()
        self._roll_sum_mask = np.roll(self._roll_sum_mask, 1)
        self._roll_sum_mask[0] = 0
        self._time_delta = np.array([(x - y).days for x, y in zip(d1, d2)])

    @accepts(object, str, list)
    def add_group(self, name, wells=[]):
        """
        Добавление группы в месторождение или
        добавление скважины в группу.

        name: строка с именем группы
        wells: список скважин
        """
        # Защита
        if not isinstance(name, str):
            raise TypeError("name must be str, not " + type(name))

        wells = [x for x in wells if x in self._well_stock]
        if name in self.groups:
            self.groups[name] += wells
        else:
            self.groups[name] = wells

    @accepts(object, str, dict)
    def add_well_data(self, wname, data={}):
        """
        Добавление векторов данных в модель скважины.
        Вектора должны соответствовать заранее определенным
        заголовкам, см. Well.parameters

        wname: строка с именем скважины
        data: словарь с векторами данных по скважине
        """
        if not wname in self._well_stock:
            self._well_stock[wname] = Well(wname)
        for parameter in data.keys():
            if len(data[parameter]) != len(self._dates):
                del data[parameter]
                self.__logger.warn("Parameter has different length: " +
                                    parameter + " ... ignoring")
                continue
            self._well_stock[wname].add_parameter(parameter, data[parameter])

    @accepts(object, dict)
    def cut_off_wells(self, cut_list):
        """
        Сокращение списка скважин, путем удаления.
        Должно быть удалено после реализации групп.

        cut_list: список выбранных скважин (которые остаются)
        """
        well_list = list(self._well_stock)
        for well in well_list:
            if well not in cut_list:
                del(self._well_stock[well])
            else:
                n = float(cut_list[well])
                if n == 1:
                    continue
                elif n < 1:
                    self._well_stock[well].reduce_rates(n)
                else:
                    raise ValueError("Bad rate")

    @accepts(object, (str, unicode), list)
    def add_parameter(self, parameter, data):
        """
        Добаление параметров месторождения,
        параметр должен соответствовать заранее
        определенному заголовку, см. Field.parameters

        parameter: строка с названием параметра
        data: вектор данных
        """
        if parameter in self.parameters:
            if parameter == "PRP":   # Грязный хак
                mask = self._time_mask[1:]
                data = np.array(data)
                data = data.compress(mask)
            self.parameters[parameter] = data

    @returns(dict)
    @accepts(object, bool, bool)
    def work_time(self, compress=False, new_only=False):
        """
        Выгрузка рабочего времени в днях по определенным в Field._dates
        датам.

        compress: выгрузка сокращенного варианта (по январям)
        new_only: выгрузка только по первому году работы скважин

        Возвращает словарь с временем работы скважин в режиме добычи и
        в режиме нагнетания.
        """

        if not self._dates:
            raise ValueError("Field dates line is not initialised")
        result = {}
        result["PROD"] = np.zeros_like(self._time_delta)
        result["INJ"] = np.zeros_like(self._time_delta)
        for well in self._well_stock.values():
            well_work_time = well.work_time()
            efac = well.recieve_parameters("EFAC")
            if efac == {}:
                efac = np.ones_like(self._time_delta)
            else:
                efac = efac["EFAC"][:-1]
            if new_only:  # ЖЕСТКИЙ РЕФАКТОР
                start, _end = well.lifecycle()
                check = self._roll_sum_mask[start["STEP"] + 1]
                n = start["STEP"]
                well_work_time["PROD"] = np.zeros_like(self._time_delta)
                well_work_time["INJ"] = np.zeros_like(self._time_delta)
                while check == self._roll_sum_mask[n + 1]:
                    if start["STATE"] == "PRODUCTION":
                        np.put(well_work_time["PROD"], n, 1)
                    elif start["STATE"] == "INJECTION":
                        np.put(well_work_time["INJ"], n, 1)
                    n += 1
            result["PROD"] += well_work_time["PROD"] * self._time_delta * efac
            result["INJ"] += well_work_time["INJ"] * self._time_delta * efac
        if compress:
            for key in result:
                result[key] = np.bincount(self._roll_sum_mask[1:] - 1,
                                            weights=result[key])
        return result

    @returns(np.ndarray)
    @accepts(object, str, str, int, int)
    def production_rate(self, fluid, mode="CML", density=1, degree=0):
        """
        Выгрузка данных о добыче/закачке по всем скважинам.
        Может быть по кумулятивной или годовой добыче,
        но данные в любом случае выгружаются только по 1 янв года.

        fluid: строка с названием флюида
        mode: режим расчета(CML-кумулятивный, YRL-годовой)
        density: плотность(в кг/м3)
        degree: степень

        Возвращает вектор добычи
        """
        if not self._dates:
            raise ValueError("Field dates line is not initialised")
        mask = self._time_mask
        if mode == "CML":
            rates = np.zeros(mask.count(True))
        else:
            rates = np.zeros(mask.count(True) - 1)
        for well in self._well_stock.values():
            rate = well.recieve_parameters(fluid, mask=mask)
            if rate == {}:
                continue
            if mode == "CML":
                rates += rate[fluid]
            elif mode == "YRL":
                rates += roll_out(rate[fluid])
            elif mode == "NEW_YRL":     # Немного костыль
                work_time = np.zeros(len(mask) - 1)
                start, _end = well.lifecycle()
                if start["STATE"] == "INJECTION":
                    continue
                np.put(work_time, start["STEP"], 1)
                work_time = np.bincount(self._roll_sum_mask[1:] - 1,
                                            weights=work_time)
                rates += roll_out(rate[fluid]) * work_time
        return density * (10 ** degree) * rates

    @returns(dict)
    @accepts(object, str, bool)
    def avg_pressure(self, pres_type, compress=False):
        """
        Выгрузка среднего давления по скважинам
        в зависимости от роли.

        pres_type: строка с типом давления
        compress: выгрузка сокращенного варианта (по январям)

        Возвращает словарь со средним давлением по добывающим и
        нагнетательным скважинам.
        """
        mask = self._time_mask[1:]
        if not self._dates:
            raise ValueError("Field dates line is not initialised")
        avg_prod_pres = np.zeros_like(self._time_delta, dtype=np.float64)
        avg_inj_pres = np.zeros_like(self._time_delta, dtype=np.float64)
        count_prod = np.zeros_like(self._time_delta)
        count_inj = np.zeros_like(self._time_delta)
        for well in self._well_stock.values():
            work_time = well.work_time()
            pressure = well.recieve_parameters(pres_type)[pres_type]
            pressure = pressure[1:]
            count_prod += work_time["PROD"]
            count_inj += work_time["INJ"]
            avg_prod_pres += work_time["PROD"] * pressure
            avg_inj_pres += work_time["INJ"] * pressure
        np.seterr(divide="ignore")
        avg_prod_pres = np.divide(avg_prod_pres, count_prod)
        avg_prod_pres[np.isnan(avg_prod_pres)] = 0
        avg_inj_pres = np.divide(avg_inj_pres, count_inj)
        avg_inj_pres[np.isnan(avg_inj_pres)] = 0
        np.seterr(divide="raise")
        if compress:
            avg_prod_pres = avg_prod_pres.compress(mask)
            avg_inj_pres = avg_inj_pres.compress(mask)
        return {"PROD": avg_prod_pres,
                "INJ": avg_inj_pres}

    @returns(dict)
    @accepts(object, bool)
    def well_fond(self, compress=False):
        """
        Расчет действующего фонда скважин в зависимости от
        роли.

        compress: выгрузка сокращенного варианта (по январям)

        Возвращает словарь с количеством действующих скважин
        работающих в режиме добычи и в режиме нагнетания
        """
        mask = self._time_mask[1:]
        result = {"INJ": np.zeros_like(self._time_delta),
                 "PROD": np.zeros_like(self._time_delta)}
        for well in self._well_stock.values():
            work_time = well.work_time()
            result["PROD"] += work_time["PROD"]
            result["INJ"] += work_time["INJ"]
        if compress:
            for key in result:
                result[key] = np.compress(mask, result[key])
        return result

    @returns(dict)
    @accepts(object, bool)
    def transfered_wells(self, compress=False):
        """
        Расчет количества переведенных скважин.

        compress: выгрузка сокращенного варианта (по январям)

        Возвращает словарь с количеством скважин переведенных из
        нагнетания в добычу(TO_PROD), переведенных из добычи в
        нагнетание(TO_INJ), переведенных в бездействие(STOP) и
        переведенных из бездействия(START).
        """
        result = {"TO_PROD": np.zeros_like(self._time_delta),
                  "TO_INJ": np.zeros_like(self._time_delta),
                  "STOP": np.zeros_like(self._time_delta),
                  "START": np.zeros_like(self._time_delta)}
        for well in self._well_stock.values():
            start, end = well.lifecycle()
            for event in well._events:
                if event.step in (start["STEP"], end["STEP"]):
                    continue
                else:
                    result[event.type][event.step] += 1
        if compress:
            for key in result:
                # КОСТЫЛЬ
                result[key] = np.bincount(self._roll_sum_mask[1:] - 1,
                                            weights=result[key])
        return result

    @returns(dict)
    @accepts(object, bool)
    def completed_wells(self, compress=False):
        """
        Расчет ввода скважин в зависимости от роли.

        compress: выгрузка сокращенного варианта (по январям)

        Возвращает словарь с временем работы скважины в режиме добычи и
        в режиме нагнетания.
        """
        result = {"PROD_IN": np.zeros_like(self._time_delta),
                   "INJ_IN": np.zeros_like(self._time_delta),
                 "PROD_OUT": np.zeros_like(self._time_delta),
                  "INJ_OUT": np.zeros_like(self._time_delta)}
        for well in self._well_stock.values():
            start, end = well.lifecycle()
            if start["STATE"] == "INJECTION":
                result["INJ_IN"][start["STEP"]] += 1
            elif start["STATE"] == "PRODUCTION":
                result["PROD_IN"][start["STEP"]] += 1
            else:
                continue    # скважина не вошла
            if end["STEP"] == -1:
                continue    # скважина осталась в работе
            elif end["STATE"] == "INJECTION":
                result["INJ_OUT"][end["STEP"]] += 1
            elif end["STATE"] == "PRODUCTION":
                result["PROD_OUT"][end["STEP"]] += 1
        if compress:
            for key in result:
                result[key] = np.bincount(self._roll_sum_mask[1:] - 1,
                                            weights=result[key])
        return result

    def clear(self):
        """Очистка объекта Field."""
        for well in self._well_stock.values():
            well.clear()
        self.parameters.clear()


def roll_out(np_lst):
    np_lst = np.roll(np_lst, -1) - np_lst
    np_lst = np.delete(np_lst, -1)
    return np_lst
