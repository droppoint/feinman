# -*- coding: UTF-8 -*-
import xlwt
import xlrd
from xlutils.copy import copy

TEMPLATE_PATH = "../templates/tables.xls"
SHEET_NAME = "RAW"


class ExcelWriter(object):

    def __init__(self):
        self._wb = xlrd.open_workbook(TEMPLATE_PATH, on_demand=True, formatting_info=True)
        self._wb = copy(self._wb)
        self._ws = self._wb.get_sheet(0)#[i.name for i in self._wb.sheets]
        # print self._ws.name
        # self._ws = self._wb.sheet_by_name(SHEET_NAME)

    def insert_row(self, line, lst, header=""):
        self._ws.write(line, 0, label=header)
        for point, data in enumerate(lst):
            self._ws.write(line, point + 1, label=data)

    def save_to_file(self, filename):
        # Нужна конструкция try
        self._wb.save(filename)
