# -*- coding: UTF-8 -*-\
import win32com.client as win32
import os.path

TEMPLATE_PATH = "templates\\tables.xlsx"
CUR_DIR = os.path.abspath(os.path.dirname(__file__))
ROOT_DIR = os.path.abspath(os.path.dirname(CUR_DIR))
TEMPLATE_PATH = os.path.abspath(os.path.join(os.path.dirname(ROOT_DIR), TEMPLATE_PATH))
SHEET_NAME = "RAW"


class ExcelCOMWriter(object):

    def __init__(self):
        self.excel = win32.gencache.EnsureDispatch('Excel.Application')
        self.excel.Visible = False
        self._wb = self.excel.Workbooks.Open(TEMPLATE_PATH)
        self._ws = self._wb.Worksheets(SHEET_NAME)

    def insert_row(self, line, lst, header=""):
        self._ws.Cells(line + 1, 1).Value = header
        for point, data in enumerate(lst):
            self._ws.Cells(line + 1, point + 2).Value = data

    def save_to_file(self, filename):
        try:
            self._wb.SaveAs(filename)
        except:
            pass
        finally:
            self.excel.Application.Quit()