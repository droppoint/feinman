# -*- coding: UTF-8 -*-
from odf.opendocument import OpenDocumentSpreadsheet
from odf.opendocument import load
from odf.table import *
from odf.text import P


TEMPLATE_PATH = "../templates/tables.ods"
SHEET_NAME = "RAW"


class OdsWriter(object):

    def __init__(self):
        self._wbs = load(TEMPLATE_PATH)
        self._wb = self._wbs.spreadsheet
        # sheet = self.get_sheet(SHEET_NAME)
        # rows = sheet.getElementsByType(TableRow)
        self._table = Table()
        # print rows

        # self._wb = copy(self._wb)
        # self._ws = self._wb.get_sheet(0)

    def insert_row(self, line, lst, header=""):
        tr = TableRow()
        for data in lst:
            tc = TableCell()
            p = P(text=data)
            tc.addElement(p)
            tr.addElement(tc)
        self._table.addElement(tr)

        # self._ws.write(line, 0, label=header)
        # for point, data in enumerate(lst):
        #     self._ws.write(line, point+1, label=data)

    # def get_sheet(self, name):
    #     for sheet in  self._wb.spreadsheet.getElementsByType(Table):
    #         if name == sheet.getAttribute("name"):
    #             return sheet


    def save_to_file(self, filename):
        self._wb.addElement(self._table)
        # Нужна конструкция try
        self._wbs.save(filename)
