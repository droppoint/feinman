# -*- coding: UTF-8 -*-

"""
СИНТАКИС
    feinman [-f, --file] [--categories][-h,--help] [-v,--verbose] [--version]

ОПИСАНИЕ
    feinman - скрипт для построения отчета в формате xlsx/ods из
    файлов расчета (*.RSM, *.DATA, *.DEV) Shlumberger Eclipse или
    (*.RSM) Roxar Tempest
    Для помощи запустите скрипт с параметрами (-h или --help)

ПРИМЕРЫ
    feinman -f C:/PRED/OBJECT1/pred.data

КОДЫ ВЫХОДА
    0 - завершено успешно
    1 - завершено с ошибкой (см. лог на предмет ошибки)

АВТОР
    Алексей Партилов <partilov@gmail.com>

ВЕРСИЯ
    0.1
"""

from __future__ import absolute_import

import logging
import os
from time import time
from datetime import datetime

import numpy as np

from Feinman.parsers.data_parser import DataParser
from Feinman.parsers.rsm_parser import RSMParser
from Feinman.parsers.deviation_parser import DevParser
from Feinman.oilfield.field import Field
from Feinman.writers.excel_COM_writer import ExcelCOMWriter


def _command_line_init():
    import optparse
    # Входные параметры из командной строки
    parser = optparse.OptionParser(formatter=optparse.TitledHelpFormatter(),
                                   usage=globals()['__doc__'],
                                   version='0.1')
#    parser.add_option('-f', '--file',
#                      action='store',
#                      help='Входной файл')
    parser.add_option('--categories',
                      action='store',
                      help='Файл в формате Yaml c именами скважин по \
                      категориям')
    parser.add_option('--conf',
                  action='store',
                  help='Файл в формате Yaml c параметрами \
                  пласта')
    parser.add_option('-v', '--verbose',
                      action="store_true",
                      help="Полный вывод всей информации")
    (options, args) = parser.parse_args()
    if len(args) != 1:
        parser.error("Укажите входной файл")
    return options, args


def _logger_init(log_level, **kw):
    import logging.handlers
    logger = logging.getLogger('Feinman')
    verbose = kw.get("verbose", None)
    logger.setLevel(log_level)
    fh = logging.handlers.RotatingFileHandler('debug.log',
                                      mode='a',
                                      maxBytes=524288)
    fh.doRollover()  # Перезаписывает лог
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
                '%(asctime)s - %(levelname)s - %(name)s - %(message)s')
    fh.setFormatter(formatter)
    if verbose:
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
    logger.addHandler(fh)
    logger.debug('Feinman launch.')
    return logger


def _read_categories_file(categories_file):
    import yaml
    logger = logging.getLogger('Feinman.categories_file')
    mas = {}
    try:
        yaml_file = open(categories_file, 'r+')
        mas = yaml.load(yaml_file, Loader=yaml.loader.BaseLoader)
    except ValueError:
        logger.error("Ошибка при считывании файла категорий")
    except IOError:
        logger.error("Файл категорий не найден")
    return mas


def _choose_parser(filename):
    #фасад
    raw_filename = filename
    filename = filename.lower()
    if filename.endswith('.data') or \
       filename.endswith('.sch'):
        return DataParser(raw_filename)
    elif filename.endswith('.rsm'):
        return RSMParser(raw_filename)
    elif filename.endswith('.dev'):
        return DevParser(raw_filename)
    else:
        logger.error("Неизвестный тип файла.")
        return None
#        raise IOError("Unknown file type")


def _check_include(block):
    files = []
    if not 'INCLUDE' in block:  # BUG
        return files
    for line in block['INCLUDE']:
        line = line[0].strip("'")
        raw_line = line
        line = line.lower()
        if line.endswith(".sch"):
            files.append(raw_line)
    return files


def filter_blocks(heap):
    data_headers = ["DENSITY", "TITLE", "WELSPECS", "DATES", "RSM_DATES",
                    "TRACER"]
    data_blocks = {}
    rsm_blocks = {}
    dev_blocks = {}
    while heap:
        block = heap.pop(0)
        header = block.keys()[0]
        if header in data_headers:
            if header in data_blocks:
                data_blocks[header] += block[header]
            else:
                data_blocks.update(block)
        if len(block.keys()) == 3:   # Плооооооохоооооо parameter data number
            well_number = block["number"] or "field"
            if block["number"] in rsm_blocks:
                rsm_blocks[well_number].update({block["parameter"]: block["data"]}) 
            else:
                rsm_blocks[well_number] = {block["parameter"]: block["data"]}
    return data_blocks, rsm_blocks, dev_blocks


def fill_template(field, excel_list, d_oil, d_water):
    template = read_template(TEMPLATE_PATH)

    if field.parameters["CATEGORY"]:
        template["CATEGORY"]["data"] = [field.parameters["CATEGORY"]]
    template["FIELDNAME"]["data"] = [field.parameters["FIELDNAME"]]
    if field.parameters["OBJECTNAME"]:
        template["OBJECTNAME"]["data"] = [field.parameters["OBJECTNAME"]]

    for parameter in field.parameters:
        if (field.parameters[parameter] != None) and (parameter in template):
            template[parameter]["data"] = field.parameters[parameter]
    template["OIL_DENS"]["data"] = [d_oil/1000]
    template["WATER_DENS"]["data"] = [d_water/1000]

    template["YEARS"]["data"] = [x.year for x in data_blocks["RSM_DATES"] if (x.month == 1) and (x.day == 1)][:-1]
    oil = field.production_rate("OIL", mode="YRL", density=d_oil, degree=-6)
    oil_trace = field.production_rate("OIL_TRACE", mode="YRL", density=d_oil, degree=-6)
    cond = field.production_rate("COND", mode="YRL", density=d_oil, degree=-6)
    sgas = field.production_rate("SGAS", mode="YRL", degree=-6)
    gas_trace = field.production_rate("GAS_TRACE", mode="YRL", degree=-6)

    # template["ANL_OIL"]["data"] = field.production_rate("OIL", mode="YRL", density=d_oil, degree=-6)
    oil_rec = oil * (oil_trace/(oil_trace+cond))
    oil_rec[np.isnan(oil_rec)] = 0
    cond_rec = oil * (cond/(oil_trace+cond))
    cond_rec[np.isnan(cond_rec)] = 0

    template["ANL_OIL"]["data"] = oil_rec
    template["ANL_COND"]["data"] = cond_rec
    # template["ANL_LIQ"]["data"] = template["ANL_OIL"]["data"] + template["ANL_COND"]["data"] + \
    #                             + field.production_rate("WATER", mode="YRL", density=d_water, degree=-6) 
    template["ANL_WAT"]["data"] = field.production_rate("WATER", mode="YRL", density=d_water, degree=-6)
    # template["ANL_COND"]["data"] = field.production_rate("COND", mode="YRL", density=d_oil, degree=-6)
    template["ANL_FGAS"]["data"] = sgas - gas_trace
    template["ANL_SGAS"]["data"] = gas_trace
    template["ANL_WATER_INJ"]["data"] = field.production_rate("WATER_I", mode="YRL", density=d_water, degree=-6)
    template["ANL_GAS_INJ"]["data"] = field.production_rate("SGAS_I", mode="YRL", degree=-6) + \
                                        field.production_rate("FGAS_I", mode="YRL", degree=-6)
    oil = field.production_rate("OIL", mode="NEW_YRL", density=d_oil, degree=-6)
    oil_trace = field.production_rate("OIL_TRACE", mode="NEW_YRL", density=d_oil, degree=-6)
    cond = field.production_rate("COND", mode="NEW_YRL", density=d_oil, degree=-6)
    sgas = field.production_rate("SGAS", mode="NEW_YRL", degree=-6)
    gas_trace = field.production_rate("GAS_TRACE", mode="NEW_YRL", degree=-6)

    oil_rec = oil * (oil_trace/(oil_trace+cond))
    oil_rec[np.isnan(oil_rec)] = 0
    cond_rec = oil * (cond/(oil_trace+cond))
    cond_rec[np.isnan(cond_rec)] = 0

    template["NEW_OIL"]["data"] = oil_rec
    template["NEW_COND"]["data"] = cond_rec
    # template["NEW_LIQ"]["data"] = template["NEW_OIL"]["data"] + template["NEW_COND"]["data"]\
    #                             + field.production_rate("WATER", mode="NEW_YRL", density=d_water, degree=-6) 
    template["NEW_WAT"]["data"] = field.production_rate("WATER", mode="NEW_YRL", density=d_water, degree=-6)
    template["NEW_FGAS"]["data"] = sgas - gas_trace
    template["NEW_SGAS"]["data"] = gas_trace

    transfered_wells = field.transfered_wells(compress=True)
    template["PR_TO_IN"]["data"] = transfered_wells["TO_INJ"]
    template["IN_TO_PR"]["data"] = transfered_wells["TO_PROD"]
    template["PR_TO_IA"]["data"] = transfered_wells["STOP"]
    template["IA_TO_PR"]["data"] = transfered_wells["START"]

    completed_wells = field.completed_wells(compress=True)
    template["DRL_PR"]["data"] = completed_wells["PROD_IN"]
    template["DRL_IN"]["data"] = completed_wells["INJ_IN"]
    template["OUT_PR"]["data"] = completed_wells["PROD_OUT"]
    template["OUT_IN"]["data"] = completed_wells["INJ_OUT"]

    well_fond = field.well_fond(compress=True)
    template["FOND_IWPR"]["data"] = well_fond["PROD"] - completed_wells["PROD_OUT"]
    template["FOND_IWIN"]["data"] = well_fond["INJ"] - completed_wells["INJ_OUT"]
    template["FOND_IW"]["data"] = template["FOND_IWPR"]["data"] + template["FOND_IWIN"]["data"]

    template["AVG_PRES"]["data"] = field.parameters["PRP"]
    avg_BHP = field.avg_pressure("BHP", compress=True)
    template["AVG_BHP_PR"]["data"] = avg_BHP["PROD"]
    template["AVG_BHP_IN"]["data"] = avg_BHP["INJ"]
    avg_BP9 = field.avg_pressure("BP9", compress=True)
    template["AVG_PRES_PR"]["data"] = avg_BP9["PROD"]
    template["AVG_PRES_IN"]["data"] = avg_BP9["INJ"]
    avg_THP = field.avg_pressure("THP", compress=True)
    template["PROD_THP"]["data"] = avg_THP["PROD"]

    work_time = field.work_time(compress=True, new_only=True)
    template["NEW_PR_TIME"]["data"] = work_time["PROD"] * 0.95
    template["NEW_IN_TIME"]["data"] = work_time["INJ"] * 0.95

    work_time = field.work_time(compress=True)
    template["PR_TIME"]["data"] = work_time["PROD"] * 0.95
    template["IN_TIME"]["data"] = work_time["INJ"] * 0.95

    for param in template.values():
        excel_list.insert_row(param["line"] - 1, param["data"], header=param["description"])

    return excel_list


# Считывание имен необходимых векторов из json файла
def read_template(path):
    import json
    template = {}
    with open(path, 'r+') as json_file:
        try:
            template = json.load(json_file)
        except ValueError:
            raise ValueError("Ошибка при считывании  \
                            шаблона отчёта")
    return template

if __name__ == '__main__':
    start_time = time()
    # \
    # Считывание параметров командной строки,
    # запуск логгера
    options, args = _command_line_init()
    log_level = logging.DEBUG
    logger = _logger_init(log_level, verbose=options.verbose)
    categories = None
    if options.categories:
        logger.debug("Подгружаю файл категорий")
        categories = _read_categories_file(options.categories)

    parsing_queue = []
    raw_file = args[0]
    parsing_queue.append(raw_file)

    if raw_file.lower().endswith('.data'):   # Теперь всё очень плохо
        rsm_file = os.path.dirname(raw_file)
        filename = raw_file.split(os.extsep, 1)[0] + ".rsm"
        rsm_file = os.path.join(rsm_file, filename)
        parsing_queue.append(rsm_file)

    heap = []
    while parsing_queue:                      # Парсинг выгружает в память
        raw_file = parsing_queue.pop(0)       # всё, что необходимо и
        dir_name = os.path.dirname(raw_file)  # закрывает файлы.
        parser = _choose_parser(raw_file)
        if not parser:
            continue
        for block in parser.next_block():
            heap.append(block)
            add_file = _check_include(block)
            add_file = [os.path.join(dir_name, x) for x in add_file]
            parsing_queue += add_file
        parser.close()

    data_blocks, rsm_blocks, dev_blocks = filter_blocks(heap)
    if data_blocks["DATES"]:
        temp_dates = []
        for date in data_blocks["DATES"]:
            if date[1].upper() in ["'JLY'", "JLY"]:
                date[1] = "JUL"
            date = date[0] + " " + date[1].strip("'") + " " + date[2]
            date = datetime.strptime(date, "%d %b %Y")
            temp_dates.append(date)
        data_blocks["DATES"] = temp_dates
        assert data_blocks["DATES"] == data_blocks["RSM_DATES"][1:]

    d_oil, d_water, d_gas = 0, 0, 0
    if data_blocks["DENSITY"]:
        density = data_blocks["DENSITY"]
        n = 0
        for line in density:
            n += 1
            d_oil += float(line[0])
            d_water += float(line[1])
            d_gas += float(line[2])
        d_oil /= n
        d_water /= n
        d_gas /= n
    else:
        raise

    # Задание модели месторождения
    title = data_blocks.get("TITLE", "No Name")
    if len(title[0]) == 1:
        field = Field(title[0][0])
    elif len(title[0]) == 2:
        field = Field(title[0][0])
        field.add_parameter("OBJECTNAME", [title[0][1]])
    else:
        raise
    field.set_dates(data_blocks["RSM_DATES"])

    if options.conf:
        logger.debug("Подгружаю файл конфигурации пласта")
        conf = _read_categories_file(options.conf)
        for parameter in conf:
            field.add_parameter(parameter, conf[parameter])

    # Наполнение модели
    for well, data in rsm_blocks.items():
        if well == "field":    #Костыль скважины должны быть отдльно а field отдельно
            for parameter in data:
                field.add_parameter(parameter, data[parameter])
            continue
        field.add_well_data(well, data)

    # Определение иерархии
    if data_blocks["WELSPECS"]:
        for well_line in data_blocks["WELSPECS"]:
            field.add_group(well_line[1].strip("'"),
                            wells=[well_line[0].strip("'")])

    CUR_DIR = os.path.abspath(os.path.dirname(__file__))
    CONFIG_DIR = os.path.abspath(os.path.join(CUR_DIR, 'config'))
    TEMPLATE_PATH = CONFIG_DIR + "/template.json"

    for well in field._well_stock.values():
        well.build_event_horizon()

    # categories cut
    if categories:
        for category in categories:
            excel_list = ExcelCOMWriter()
            wells = categories[category]
            cut_field = Field(field.name)
            cut_field.set_dates(field._dates)
            # cut_field.add_parameter("OBJECTNAME", field.parameters["OBJECTNAME"])
            for parameter in field.parameters:
                if (field.parameters[parameter] != None) and (parameter != "PRP"):
                    cut_field.add_parameter(parameter, field.parameters[parameter])
            cut_field.parameters["PRP"] = field.parameters["PRP"]  # ГРЯЗНЫЙ ХАК
            cut_field.add_parameter("CATEGORY", [category])
            cut_field._well_stock = field._well_stock.copy()
            cut_field.cut_off_wells(wells)
            excel_list = fill_template(cut_field, excel_list, d_oil, d_water)
            excel_list.save_to_file(category + ".xlsx")
    else:
        excel_list = ExcelCOMWriter()
        excel_list = fill_template(field, excel_list, d_oil, d_water)
        excel_list.save_to_file("test.xlsx")

    field.clear()
    print "Benchmark: ", time() - start_time, "seconds"
